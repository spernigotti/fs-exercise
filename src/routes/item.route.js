const express = require('express');
const { asyncHandler, uploadHandler } = require('../middlewares');

const router = express.Router();

const controller = require('../controllers/item.controller');

router.get('/items', asyncHandler(controller.findAll));

router.get('/items/:id', asyncHandler(controller.findOne));

router.post('/items', uploadHandler.single('picture'), asyncHandler(controller.create));

router.put('/items/:id', uploadHandler.single('picture'), asyncHandler(controller.update));

router.delete('/items/:id', asyncHandler(controller.delete));

module.exports = router;
