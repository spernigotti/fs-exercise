const DataInvalid = require('./DataInvalid');
const InternalError = require('./InternalError');
const NotFound = require('./NotFound');

module.exports = {
	DataInvalid,
	InternalError,
	NotFound
};
