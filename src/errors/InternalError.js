class InternalError extends Error {
	constructor(message) {
		super(message);
		this.name = 'Internal Error';
		this.statusCode = 500;
	}
}

module.exports = InternalError;
