class DataInvalid extends Error {
	constructor(message) {
		super(message);
		this.name = 'Data Invalid';
		this.statusCode = 400;
	}
}

module.exports = DataInvalid;
