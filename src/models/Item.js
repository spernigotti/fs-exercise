/* eslint-disable no-param-reassign */
const mongoose = require('mongoose');

const { Schema } = mongoose;

const DESCRIPTION_MAX_LENGTH = 300;

const Item = new Schema({
	description: { type: String, maxlength: DESCRIPTION_MAX_LENGTH, required: true },
	// Image file name
	image: { type: String, required: true },
	// Item position in the list
	position: { type: mongoose.Decimal128, required: true }
});

Item.index({ pos: 1 });

// Rename _id to id and remove __v fields in responses
Item.set('toJSON', {
	transform(_, doc) {
		doc.id = doc._id;
		delete doc.__v;
		delete doc._id;

		doc.position = Number(doc.position.toString());
	}
});
Item.set('toObject', {
	transform(_, doc) {
		doc.id = doc._id;
		delete doc.__v;
		delete doc._id;

		doc.position = Number(doc.position.toString());
	}
});

module.exports = mongoose.model('Item', Item);
