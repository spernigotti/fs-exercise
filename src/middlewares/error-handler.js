const mongoose = require('mongoose');

const { ValidationError } = mongoose.Error;

function statusByError(error) {
	if(error instanceof ValidationError)
		return 400;

	return error.statusCode || 500;
}

function errorHandler(error, req, res, next) { // eslint-disable-line no-unused-vars
	res.status(statusByError(error));

	if(req.accepts('json'))
		return res.send({ error: error.message });

	res.type('txt').send(error.message);
}

module.exports = errorHandler;
