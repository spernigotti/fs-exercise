const asyncHandler = require('./async-handler');
const errorHandler = require('./error-handler');
const uploadHandler = require('./upload-handler');

module.exports = {
	asyncHandler,
	errorHandler,
	uploadHandler
};
