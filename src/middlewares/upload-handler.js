const path = require('path');
const multer = require('multer');
const { DataInvalid } = require('../errors');

const DESTINATION = 'public/images/items';
const VALID_FILETYPES = /^\.(jpg|jpeg|png|gif)$/i;
const VALID_MIMETYPES = /^(image\/gif|image\/jpeg|image\/png|image\/image\/gif)$/i;

const storage = multer.diskStorage({
	destination(req, file, cb) {
		cb(null, DESTINATION);
	},
	filename(req, file, cb) {
		const extenstion = path.extname(file.originalname);
		cb(null, `${Date.now()}${extenstion}`);
	}
});

const uploadHandler = multer({
	storage,
	/**
	 * Validate file
	 * @param {object} req
	 * @param {object} file
	 * @param {function} cb
	 */
	fileFilter(req, file, cb) {
		const hasValidMimetype = VALID_MIMETYPES.test(file.mimetype);
		const hasValidExtension = VALID_FILETYPES.test(path.extname(file.originalname));
		console.log('hasValidMimetype', hasValidMimetype, hasValidExtension, path.extname(file.originalname), file.mimetype);

		if(hasValidMimetype && hasValidExtension)
			return cb(null, true);

		cb(new DataInvalid(`Error: File upload only supports the following filetypes - ${VALID_FILETYPES}`));
	}
});

module.exports = uploadHandler;
