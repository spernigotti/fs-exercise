const mongoose = require('mongoose');
const fs = require('fs');
const path = require('path');

const IMAGES_PATH = 'public/images/items';

/**
 * Validate if the id is an Mongodb ObjectId
 * @param {string} id
 * @returns {boolean}
 */
const isObjectId = id => mongoose.Types.ObjectId.isValid(id);

/**
 * Delete an item image file
 * @param {string} fileName
 */
const deleteItemImage = (fileName) => {
	fs.unlink(path.join(IMAGES_PATH, fileName), () => null);
};

module.exports = {
	isObjectId,
	deleteItemImage
};
