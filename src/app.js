const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const path = require('path');
const itemRoute = require('./routes/item.route');
const errorHandler = require('./middlewares/error-handler');
const config = require('../database.config');

const PORT = 3000;
const RECONNECT_TIMEOUT = 3000;
const PUBLIC_PATH = path.join(__dirname, '..', 'public');

class App {
	constructor() {
		this.initialize();
	}

	async initialize() {
		await this.initalizeDatabase();
		this.initalizeServer();
	}

	/**
	 * @returns {Promise}
	 */
	initalizeDatabase() {
		return new Promise((resolve) => {
			const connect = () => {
				mongoose.connect(config.DB, { auto_reconnect: true });
			};

			connect();
			mongoose.Promise = global.Promise;
			this.db = mongoose.connection;

			this.db.on('error', (error) => {
				console.error(`MongoDB connection error: ${error}`);
				mongoose.disconnect();
			});

			this.db.on('disconnected', () => {
				console.error(`MongoDB disconnected! Reconnecting in ${RECONNECT_TIMEOUT / 1000}s...`);

				/* Waiting for mongo service to be up, depends_on does not wait for
				mongo container to be ready before starting, it will only launch it first */
				setTimeout(() => connect(), RECONNECT_TIMEOUT);
			});

			this.db.once('open', resolve);
		});
	}

	initalizeServer() {
		const server = express();
		this.server = server;

		server.use(logger('dev'));

		server.use(bodyParser.urlencoded({ extended: true }));
		server.use(bodyParser.json());

		server.use(express.static(PUBLIC_PATH));

		server.use(itemRoute);
		server.use(errorHandler);

		server.listen(PORT, console.log.bind(null, 'Server is running on PORT:', PORT));
	}
}

new App();
