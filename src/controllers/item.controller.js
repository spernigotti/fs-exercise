const { DataInvalid, NotFound } = require('../errors');
const { isObjectId, deleteItemImage } = require('../utils');
const Item = require('../models/Item');

const itemController = {
	/**
	 * Create an item
	 * @param {object} req - Express request object
	 * @param {object} res -  Express response object
	 */
	async create(req, res) {
		if(!req.file)
			throw new DataInvalid('Picture must be sent.');

		const item = new Item({
			description: req.body.description,
			image: req.file.filename,
			position: req.body.position
		});

		const result = await item.save();
		res.json(result);
	},

	/**
	 * Find all items sorting by position
	 * @param {object} req - Express request object
	 * @param {object} res -  Express response object
	 */
	async findAll(req, res) {
		const result = await Item.find({}).sort({ position: 1 });
		res.json(result);
	},

	/**
	 * Find item by id
	 * @param {object} req - Express request object
	 * @param {object} res -  Express response object
	 */
	async findOne(req, res) {
		const { id } = req.params;

		if(!isObjectId(id))
			throw new NotFound('Item not found');

		const result = await Item.findById(req.params.id);

		if(!result)
			throw new NotFound('Item not found');

		res.json(result);
	},

	/**
	 * Update an item
	 * @param {object} req - Express request object
	 * @param {object} res -  Express response object
	 */
	async update(req, res) {
		const { id } = req.params;
		const { description, position } = req.body;

		if(!isObjectId(id))
			throw new NotFound('Item not found');

		const data = {};

		if(description)
			data.description = description;

		if(position)
			data.position = position;

		if(req.file)
			data.image = req.file.filename;

		const result = await Item.findByIdAndUpdate(id, { $set: data });

		if(!result)
			throw new NotFound('Item not found');

		res.json({ ...result.toObject(), ...data });

		// If update the image, delete the previous one
		if(req.file)
			deleteItemImage(result.image);
	},

	/**
	 * Delte an item
	 * @param {object} req - Express request object
	 * @param {object} res -  Express response object
	 */
	async delete(req, res) {
		const { id } = req.params;

		if(!isObjectId(id))
			throw new NotFound('Item not found');

		const result = await Item.findByIdAndRemove(id);

		if(!result)
			throw new NotFound('Item not found');

		res.json({ message: `Item ${id} deleted.` });

		deleteItemImage(result.image);
	}
};


module.exports = itemController;
