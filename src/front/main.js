import $ from 'cash-dom';
import Sortable from 'sortablejs';
import '@babel/polyfill';
import './styles/main.scss';

import api from './api';
import { move, readAndShow } from './utils';
import elements from './elements';
import renderer from './renderer';

const INITIAL_POSITION = 100;

class App {
	constructor() {
		this.items = [];
		this.bindEvents();
		this.makeItemsSortable();
		this.fetchItems();
	}

	/**
	 * Get last position of the items
	 */
	get lastPosition() {
		if(this.items.length)
			return this.items[this.items.length - 1].position;

		return INITIAL_POSITION;
	}

	/**
	 * Get the total of items
	 */
	get total() {
		return this.items.length;
	}

	bindEvents() {
		$(document)
			.on('submit', '.item-form:not(.item-form--edit)', this.handleSubmitNew.bind(this))
			.on('submit', '.item-form--edit', this.handleSubmitEdit.bind(this))
			.on('click', '.item__delete', this.handleDeleteItem.bind(this))
			.on('click', '.item__edit', this.handleShowEditForm.bind(this));

		$(elements.pictureFile).on('change', this.handleSelectPicture.bind(this));
		$(elements.cancelEdit).on('click', this.handleCancelEdit.bind(this));
	}

	async fetchItems() {
		this.items = await api.getItems();
		renderer.renderItems(this.items);
		this.renderTotal();
	}

	/**
	 * Make items sortable with drag & drop
	 */
	makeItemsSortable() {
		new Sortable(elements.itemList, {
			onUpdate: this.handleReorderItem.bind(this)
		});
	}

	/**
	 * Make formData from input values for edit item
	 * @returns {object} formData
	 */
	makeEditItemFormData() {
		const { pictureFile, description } = elements;
		const data = new FormData();

		data.append('description', description.value);

		if(pictureFile.files.length)
			data.append('picture', pictureFile.files[0]);

		return data;
	}

	/**
	 * Make formData from input values for new item
	 * @returns {object} formData
	 */
	makeNewItemFormData() {
		const data = this.makeEditItemFormData();
		data.append('position', this.lastPosition + 100);

		return data;
	}

	resetFormValues() {
		const { pictureFile, description } = elements;
		description.value = '';
		pictureFile.value = '';

		$(elements.picture).attr('src', '');
	}

	async handleSubmitNew(event) {
		event.preventDefault();
		const data = this.makeNewItemFormData();

		const item = await api.addItem(data);

		this.items = [...this.items, item];
		renderer.renderItem(item);

		this.resetFormValues();
		this.renderTotal();
	}

	async handleSubmitEdit(event) {
		event.preventDefault();
		const data = this.makeEditItemFormData();

		await this.updateItem(this.editItemId, data);

		this.restoreNewItemForm();
	}

	handleSelectPicture() {
		readAndShow(elements.pictureFile, elements.picture);
	}

	/**
	 * Reoder an item
	 * @param {Object} event
	 * @param {number} event.newIndex
	 * @param {number} event.oldIndex
	 * @memberof App
	 */
	handleReorderItem({ newIndex, oldIndex }) {
		this.items = move(this.items, oldIndex, newIndex);

		this.updatePosition(newIndex);
	}

	async handleDeleteItem(event) {
		const $item = $(event.target).closest('.item');
		const id = $item.data('id');
		await api.deleteItem(id);

		renderer.deleteItem($item);

		const index = this.items.findIndex(item => item.id === id);
		this.items.splice(index, 1);

		// If item is being edited
		if(this.editFormActive && this.editItemId === id)
			this.restoreNewItemForm();

		this.renderTotal();
	}

	/**
	 * Show edit form
	 * @param {Objec} event
	 */
	handleShowEditForm(event) {
		const id = $(event.target).closest('.item').data('id');
		this.editItemId = id;
		this.editFormActive = true;
		const item = this.items.find(currentItem => currentItem.id === id);

		elements.description.value = item.description;
		$(elements.form).addClass('item-form--edit');
		$(elements.picture).attr('src', this.makeItemImageSrc(item.image));
		$(elements.pictureFile).removeAttr('required');
	}

	restoreNewItemForm() {
		$(elements.form).removeClass('item-form--edit');
		$(elements.picture).attr('src', '');
		$(elements.pictureFile).prop('required', true);
		elements.description.value = '';
		this.editFormActive = false;
	}

	handleCancelEdit(event) {
		event.preventDefault();

		this.restoreNewItemForm();
	}

	async updateItem(id, data) {
		const index = this.items.findIndex(currentItem => currentItem.id === id);

		const item = this.items[index];
		const result = await api.updateItem(item.id, data);

		this.items[index] = result;

		renderer.updateItem(result);

		return result;
	}

	/**
	 *	Update position of an item
	 * @param {number} itemIndex
	 */
	updatePosition(itemIndex) {
		const prevPosition = itemIndex === 0 ? 0 : this.items[itemIndex - 1].position;
		const nextPosition = (itemIndex + 1) === this.items.length
			? this.lastPosition + 100 : this.items[itemIndex + 1].position;

		const position = (prevPosition + nextPosition) / 2;
		this.updateItem(this.items[itemIndex].id, { position });
	}

	/**
	 * @param {string} image file name
	 * @returns {string}
	 */
	makeItemImageSrc(image) {
		return `/images/items/${image}`;
	}

	renderTotal() {
		renderer.renderTotal(this.total);
	}
}

new App();
