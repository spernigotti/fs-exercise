
const handleErrors = (response) => {
	if(!response.ok)
		throw Error(response.statusText);

	return response;
};

const toJson = res => res.json();

const api = {
	endpoint: '/items',

	getItems() {
		return fetch(this.endpoint)
			.then(handleErrors)
			.then(toJson);
	},

	addItem(data) {
		return fetch(this.endpoint, {
			method: 'POST',
			body: data
		})
			.then(handleErrors)
			.then(toJson);
	},

	updateItem(id, data) {
		const headers = {};
		const isFormData = data instanceof FormData;
		if(!isFormData)
			headers['Content-Type'] = 'application/json';

		return fetch(`${this.endpoint}/${id}`, {
			method: 'PUT',
			body: isFormData ? data : JSON.stringify(data),
			headers
		}).then(handleErrors).then(toJson);
	},

	deleteItem(id) {
		return fetch(`${this.endpoint}/${id}`, {
			method: 'DELETE',
		}).then(handleErrors);
	}
};

export default api;
