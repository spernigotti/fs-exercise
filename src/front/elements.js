export default {
	form: document.getElementById('js-form'),
	pictureFile: document.getElementById('js-picture-file'),
	picture: document.getElementById('js-picture'),
	description: document.getElementById('js-description'),
	itemList: document.getElementById('js-item-list'),
	total: document.getElementById('js-total'),
	cancelEdit: document.getElementById('js-cancel-edit')
};
