import $ from 'cash-dom';

/**
 * Move element in array
 * @param {Array} arr
 * @param {number} from
 * @param {number} to
 */
export function move(arr, from, to) {
	const result = arr.slice();
	result.splice(to, 0, result.splice(from, 1)[0]);

	return result;
}

/**
 * Read an image file and show it on image element
 * @param {Object} inputFile
 * @param {Object} image - img element
 */
export function readAndShow(inputFile, 	image) {
	const reader = new FileReader();

	reader.onload = (event) => {
		$(image).attr('src', event.target.result);
	};

	reader.readAsDataURL(inputFile.files[0]);
}

export default { move };
