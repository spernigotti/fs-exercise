import $ from 'cash-dom';
import elements from './elements';

const renderer = {
	/**
	 * @param {string} image name
	 */
	makeItemImageSrc(image) {
		return `/images/items/${image}`;
	},

	/**
	 * Delete item
	 * @param {Object} $cash object element
	 */
	deleteItem($item) {
		$item.remove();
	},

	/**
	 * Update item
	 * @param {Object} item
	 * @param {number} item.id
	 * @param {string} item.description
	 * @param {string} item.image
	 */
	updateItem(item) {
		const $item = $(`.item[data-id="${item.id}"]`);
		$item.find('.item__description').text(item.description);
		$item.find('.item__img').attr('src', this.makeItemImageSrc(item.image));
	},

	/**
	 * Render items
	 * @param {Array<Object>} items
	 */
	renderItems(items) {
		items.forEach(this.renderItem, this);
	},

	/**
	 * Render an item
	 * @param {Object} item
	 * @param {number} item.id
	 * @param {string} item.description
	 * @param {string} item.image
	 */
	renderItem({ id, description, image }) {
		const html = `
			<li class="item" data-id="${id}">
				<img class="item__img" src="${this.makeItemImageSrc(image)}" />
				<div class="item__description">${description}</div>
				<div class="item__actions">
					<button class="item__edit button button--icon"><img class="button__icon" src="/images/edit.svg" /></button>
					<button class="item__delete button button--icon"><img class="button__icon" src="/images/remove.svg" /></button>
				</div>
			</li>
		`;

		$(elements.itemList).append(html);
	},

	/**
	 * Render Total
	 * @param {number} total
	 */
	renderTotal(total) {
		const s = total !== 1 ? 's' : '';

		$(elements.total).text(`${total} item${s}`);
	}
};

export default renderer;
