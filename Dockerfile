FROM node:10.13-alpine AS base
RUN mkdir -p /src/app
WORKDIR /src/app
COPY package*.json /src/app/

FROM base AS production
RUN npm install --only=production
EXPOSE 3000
COPY . .
CMD ["node", "."]

FROM base AS development
RUN npm install
RUN npm install -g nodemon
COPY . .
CMD ["nodemon", "."]

VOLUME /src/app
VOLUME /src/app/node_modules
