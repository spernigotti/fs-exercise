module.exports = {
	extends: '@spernigotti/eslint-config',
	env: {
		node: true
	},
	rules: {
		'no-new': 'off',
		'no-underscore-dangle': ['error', { allow: ['_id', '__v'] }]
	},
	overrides: {
		files: ['src/front/*.js'],
		env: {
			node: false,
			browser: true
		},
		globals: {
			'$': false,
			Sortable: false
		},
		rules: {
			'class-methods-use-this': 'off'
		}
	}
}