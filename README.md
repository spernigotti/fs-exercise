# FS Exercise
Nodejs item list application.

## Getting started

To get the app running locally:
```sh
docker-compose up
```

The app will be running at http://localhost:3000


## Development
To run development mode you need to use `docker-compose.development.yml`
```
docker-compose -f docker-compose.yml -f docker-compose.development.yml up
```

or if you have `npm`

```
npm run dev
```
