const path = require('path');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');

const SRC_FOLDER = path.resolve(__dirname, './src/front');
const PUBLIC_FOLDER = path.resolve(__dirname, './public');

module.exports = {
	entry: path.resolve(SRC_FOLDER, './main.js'),
	output: {
		path: PUBLIC_FOLDER,
		filename: 'main.bundle.js'
	},
	optimization: {
		minimizer: [
			new OptimizeCSSAssetsPlugin(),
			new TerserPlugin()
		]
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: ['babel-loader']
			},
			{
				test: /\.scss$/,
				use: [
					MiniCssExtractPlugin.loader,
					'css-loader',
					'sass-loader'
				]
			}
		]
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: 'styles.css'
		})
	]
};
